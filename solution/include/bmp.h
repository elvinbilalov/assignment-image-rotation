#ifndef _BMP_H_
#define _BMP_H_

#include <stdio.h>
#include  <stdint.h>
#include "image.h"

struct bmp_header {
    struct __attribute__((packed)) {
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bfOffBits;
    } BITMAPFILEHEADER;

    struct __attribute__((packed)) {
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
    } BITMAPINFO;
};


/*  deserializer   */
enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_INTERNAL_ERROR
  };



/*  serializer   */
enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
  // TODO: коды для других ошибок
};

enum read_status from_bmp( FILE* in, struct image* img );
enum write_status to_bmp( FILE* out, struct image const* img );

#endif
