#ifndef _UTIL_H_
#define _UTIL_H_

#include <stdio.h>

FILE* open_file(const char* file_name);
FILE* create_file(const char* file_name);
void close_file(FILE* file);
size_t file_size(FILE* file);

#endif
