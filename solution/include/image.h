#ifndef _IMAGE_H_
#define _IMAGE_H_

#include <stddef.h>
#include <inttypes.h>

struct pixel { uint8_t b, g, r; };

struct image {
     size_t width, height;
     struct pixel* data;
   };

struct point {
    size_t x, y;
};

struct image image_create(size_t width, size_t height);

void image_destroy(struct image* img);

#endif
