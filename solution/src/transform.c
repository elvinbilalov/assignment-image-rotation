#include "transform.h"

static struct point points_rolled_left(size_t x, size_t y, size_t height);


struct image image_turn_left(const struct image img) {

    struct image turn_image = image_create(img.height, img.width);

    if(!turn_image.data) {
        return turn_image;
    }

    // image processing
    for(size_t i = 0; i < img.height; i++){
        for(size_t j = 0; j < img.width; j++){
            struct pixel px = *(img.data + img.width * i + j);
            struct point new_point = points_rolled_left(i,j, img.height);
            *(turn_image.data + turn_image.width * new_point.x + new_point.y) = px;
        }
    }
    return turn_image;
}

static struct point points_rolled_left(size_t x, size_t y, size_t height){
    return (struct point) {y, height - x - 1};
}
