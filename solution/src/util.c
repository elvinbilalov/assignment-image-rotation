#include "util.h"

FILE* open_file(const char* file_name) {
    return fopen(file_name, "rb");
}

FILE* create_file(const char* file_name) {
    return fopen(file_name, "w+b");
}

void close_file(FILE* file) {
     fclose(file);
}

size_t file_size(FILE* file){
    fseek(file, 0L, SEEK_END);
    return ftell(file);
}
