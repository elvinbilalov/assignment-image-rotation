#include "bmp.h"
#include "transform.h"
#include "util.h"
#include <stdio.h>
#include <stdlib.h>





static void clear_context(FILE* in_file, FILE* out_file, struct image* in_image, struct image* out_image);

static void usage(int argc);


int main(int argc, char** argv) {
    usage(argc);
    char* input_file_name = argv[1];
    char* output_file_name = argv[2];
    FILE* source_file = NULL;
    FILE* destiantion_file = NULL;
    struct image image = {0, 0, NULL};
    struct image rotated_image = {0, 0, NULL};

    source_file = open_file(input_file_name);

    if(!source_file) {
        fprintf(stderr, "File Does Not Exist\n");
        clear_context(source_file, destiantion_file, &image, &rotated_image);
        exit(1);
    }

    enum read_status read_result = from_bmp( source_file, &image );
    
    if(read_result != READ_OK){
        switch(read_result){
        	case READ_INVALID_SIGNATURE:
        		fprintf(stderr, "Invalid BMP Siganture File\n");
        		break;
        	case READ_INVALID_BITS:
        		fprintf(stderr, "Invalid BMP Data\n");
        		break;
        	case READ_INVALID_HEADER:
        		fprintf(stderr, "Invalid BMP Header File\n");
        		break;
        	case READ_INTERNAL_ERROR:
        		fprintf(stderr, "Internal Error\n");
        		break;
        	default: 
        		fprintf(stderr, "Unknown Error\n");
        		break;
        } 

        clear_context(source_file, destiantion_file, &image, &rotated_image);
        exit(1);
    }

    rotated_image = image_turn_left(image);

    if (!rotated_image.data){
        fprintf(stderr, "File Transfomation Error\n");
        clear_context(source_file, destiantion_file, &image, &rotated_image);
        exit(1);
    }

    destiantion_file = create_file(output_file_name);
    enum write_status write_result = to_bmp( destiantion_file, &rotated_image );

    if(write_result != WRITE_OK) {
        fprintf(stderr, "Can't create file\n");
        clear_context(source_file, destiantion_file, &image, &rotated_image);
        exit(1);
    }

    clear_context(source_file, destiantion_file, &image, &rotated_image);

    printf("Transformation Was Successful\n");

    return 0;
}

static void clear_context(FILE* in_file, FILE* out_file, struct image* in_image, struct image* out_image) {
    if (in_file) { close_file(in_file); }
    if (out_file) { close_file(out_file); }
    image_destroy(in_image);
    image_destroy(out_image);
}

static void usage(int argc){
    if(argc < 3){
        fprintf(stderr, "Usage: bmp_rotate INPUT_FILE OUTPUT_FILE\n");
        exit(1);
    }
}
