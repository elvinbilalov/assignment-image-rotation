#include "bmp.h"
#include "util.h"

#define BMP_SIGNATURE 0x4D42
#define PADDING 4

static struct bmp_header create_header(const struct image* img);
static size_t padding(size_t width,size_t byte_per_pixel);
static enum read_status bmp_header_read(FILE* in, struct bmp_header* header);

enum read_status from_bmp( FILE* in, struct image* img ) {

    struct bmp_header header = {0};

    enum read_status res = bmp_header_read(in, &header);

    if(res != READ_OK) return res;

    // контейнер для картинки
    *img = image_create(header.BITMAPINFO.biWidth,header.BITMAPINFO.biHeight);
    if(!img->data) {
        return READ_INTERNAL_ERROR;
    }
//----------------------------------------------------------------------------

    fseek (in, header.BITMAPFILEHEADER.bfOffBits, SEEK_SET);
    size_t padding_size = padding(img->width, sizeof(struct pixel));

    for(size_t i = 0; i < img->height; i++){
        fread(img->data + img->width * i, img->width * sizeof(struct pixel), 1, in);
        fseek (in, padding_size, SEEK_CUR);
    }

    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    struct bmp_header new_header = create_header(img);
    if(fwrite(&new_header, sizeof(struct bmp_header), 1, out) < 1) return WRITE_ERROR;

    uint8_t padding_value[3] = {0};
    size_t padding_size = padding(img->width, sizeof(struct pixel));

    for(size_t i = 0; i < img->height; i++){
        if(fwrite(img->data + img->width * i, img->width * sizeof(struct pixel), 1, out) < 1) return WRITE_ERROR;
        if(padding_size != 0 && fwrite(padding_value, padding_size, 1, out) < 1) return WRITE_ERROR;
    }

    return WRITE_OK;
}

static struct bmp_header create_header(const struct image* img) {

    size_t size_of_data = (img->width * sizeof(struct pixel) + padding(img->width, sizeof(struct pixel))) * img->height;// Размер пиксельных данных в байтах.

    struct bmp_header header = {0};

    header.BITMAPFILEHEADER.bfType = BMP_SIGNATURE;
    header.BITMAPFILEHEADER.bfileSize =  sizeof(struct bmp_header) + size_of_data;
    header.BITMAPFILEHEADER.bfReserved = 0;
    header.BITMAPFILEHEADER.bfOffBits = sizeof(struct bmp_header); // Мы пропускаем Colour table and Gap1, поэотму положение пиксельных данных относительно начала данной структуры будет равно размеру header'а

    header.BITMAPINFO.biSize = sizeof(header.BITMAPINFO);
    header.BITMAPINFO.biWidth = img->width;
    header.BITMAPINFO.biHeight = img->height;
    header.BITMAPINFO.biPlanes = 1; // В BMP допустимо только значение 1
    header.BITMAPINFO.biBitCount = sizeof(struct pixel) * 8;
    header.BITMAPINFO.biCompression = 0; // Храним в двумерном массиве, поэтому значение 0
    header.BITMAPINFO.biSizeImage = size_of_data; // Размер пиксельных данных в байтах. Может быть обнулено если хранение осуществляется двумерным массивом.
    header.BITMAPINFO.biXPelsPerMeter = 0; //
    header.BITMAPINFO.biYPelsPerMeter = 0; //
    header.BITMAPINFO.biClrUsed = 0; // Таблицы цветов нет
    header.BITMAPINFO.biClrImportant = 0;

    return header;
}

static size_t padding(size_t width,size_t byte_per_pixel) {
    size_t line_size = width * byte_per_pixel;

    return line_size % PADDING != 0
        ? PADDING - line_size % PADDING
        : 0;
}

static enum read_status bmp_header_read(FILE* in, struct bmp_header* header){

    if(fread(header, sizeof(struct bmp_header), 1, in) != 1){ // читаем заголовок из файла и проверяем его на ошибку чтения
        return READ_INVALID_HEADER;
    }

    if(header->BITMAPFILEHEADER.bfType != BMP_SIGNATURE) {
        return READ_INVALID_SIGNATURE;
    }

    if(header->BITMAPFILEHEADER.bfileSize != file_size(in)){
        return READ_INVALID_BITS;
    }

    return READ_OK;
}
